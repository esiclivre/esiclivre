# e-SIC Livre #

O e-SIC Livre é um sistema Eletrônico do Serviço de Informação ao Cidadão (e-SIC) que permite aos seus usuários solicitar informações políticas e sociais de órgãos públicos, seguindo os parâmetros determinados na [LAI - Lei de Acesso à Informação (Lei nº 12.527/2011)](http://www.planalto.gov.br/ccivil_03/_ato2011-2014/2011/lei/l12527.htm).

O software foi desenvolvido inicialmente pela Secretaria Municipal de Planejamento, Fazenda e Tecnologia da Informação (SEMPLA - Natal/RN) e disponibilizado sobre a licença [GNU General Public License, version 2](https://www.gnu.org/licenses/old-licenses/gpl-2.0.html).

Este repositório é um fork do projeto originalmente disponibilizado em [https://softwarepublico.gov.br/gitlab/e-sic-livre/e-sic-livre](https://softwarepublico.gov.br/gitlab/e-sic-livre/e-sic-livre).
